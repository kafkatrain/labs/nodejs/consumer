const { Kafka } = require('kafkajs');
const axios = require('axios');
const qs = require('qs');

const brokers = process.env.BROKER_LIST || "localhost:9092";
const grupoConsumo = process.env.GROUP_ID || "meu.grupo";
const topico = process.argv[2] || "meu.topico";

const client_id = process.env.OAUTH_CLIENT_ID;
const client_secret = process.env.OAUTH_CLIENT_SECRET;
const token_endpoint = process.env.OAUTH_TOKEN_ENDPOINT_URI || 'https://keycloak.keycloak:30443/auth/realms/master/protocol/openid-connect/token';

console.log("Brokers.........: ", brokers);
console.log("Grupo de consumo: ", grupoConsumo);
console.log("Topico..........: ", topico);

const get_token = {
  client_id: client_id,
  client_secret: client_secret,
  scope: 'email',
  grant_type: 'client_credentials'
};

const axios_config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
}

const kafka = new Kafka({
  clientId: 'app.example',
  brokers: [brokers],
  // authenticationTimeout: 1000,
  // reauthenticationThreshold: 10000,
  ssl: true,
  sasl: {
    mechanism: 'oauthbearer',
    oauthBearerProvider: async () => {

      const token = await axios.post(token_endpoint, qs.stringify(get_token), axios_config);
      //console.log(token.data.access_token);

      return {
        value: token.data.access_token
      }
    }
  },
});

async function run() {  
  const consumer = kafka.consumer({ groupId: grupoConsumo })

  await consumer.connect()
  await consumer.subscribe({ topic: topico, fromBeginning: true })

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
          key: (message.key || "null").toString(),
          value: message.value.toString(),
          headers: message.headers,
      })
    },
  })
}

run()
  .then((result) => {})
  .catch(console.error)