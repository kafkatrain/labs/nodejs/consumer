const Kafka = require("kafka-node");

const brokers = process.env.BROKER_LIST || "localhost:9092";
const grupoConsumo = process.env.GROUP_ID || "meu.grupo";
const efetivar = process.env.COMMIT || "sim";
const topico = process.argv[2] || "meu.topico";

console.log("Brokers.........: ", brokers);
console.log("Grupo de consumo: ", grupoConsumo);
console.log("Topico..........: ", topico);

const client = new Kafka.KafkaClient({kafkaHost: brokers});

const consumer = new Kafka.Consumer(
    client,
    [{topic : topico}],
    {
        groupId    : grupoConsumo,
        autoCommit : false,
        encoding   : "utf8"
    }
);

consumer.on("message", (message) => {
    console.log(JSON.stringify(message));

    if(efetivar === "sim") {
        consumer.commit((err, data) => {
            if(err) {
                console.error("Erro no commit", err);
            } else {
                console.log("Efetivado ", data);
            }
        });
    }
});

consumer.on("error", (err) => {
    console.error("Erros ", err);
});

consumer.on("offsetOutOfRange", (topic) => {
    console.log(topic);
});