const Kafka = require("node-rdkafka");

const brokers = process.env.BROKER_LIST || "localhost:9092";
const grupoConsumo = process.env.GROUP_ID || "meu.grupo";
const topico = process.argv[2] || "meu.topico";

console.log("Brokers.........: ", brokers);
console.log("Grupo de consumo: ", grupoConsumo);
console.log("Topico..........: ", topico);

const consumer = new Kafka.KafkaConsumer({
    "group.id"                 : grupoConsumo,
    "metadata.broker.list"     : brokers,
    "enable.auto.commit"       : false,
    "enable.auto.offset.store" : false,
    "auto.offset.reset"        : "earliest"
}, {});

// Em caso de erros, mostrá-los no console
consumer.on("event.error", (err) => {
    console.error(err);
});

// Conectar ao broker
consumer.connect();

// Quanto estiver pronta a conexão, podemos consumir
consumer.on("ready", () => {

    // Consumir um tópico
    consumer.subscribe([topico]);

    // Iniciar o recebimento dos registros
    //consumer.consume();

    // Read one message every 1000 milliseconds
    setInterval(function() {
      consumer.consume(10);
    }, 1000);
});

// Quando receber dados
consumer.on("data", (record) => {

    console.log(JSON.stringify(record));

});

setTimeout(function() {
  consumer.disconnect();
}, 30000);